import javafx.util.Pair;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringCalculator {

    private static final String MESSAGE_NEGATIVE_NUMBERS_NOT_ALLOWED = "negatives not allowed";
    private static final List<String> DEFAULT_DELIMITER_LIST;

    static {
        DEFAULT_DELIMITER_LIST = new LinkedList<>();
        DEFAULT_DELIMITER_LIST.add(",");
        DEFAULT_DELIMITER_LIST.add("\n");
    }

    public int add(String input) {
        int result = 0;

        if (input != null && input.length() > 0) {
            String[] parameters = splitParameters(input);
            List<Integer> validatedParameterList = validateParameters(parameters);

            result =
                validatedParameterList
                    .stream()
                    .filter(value -> value <= 1000)
                    .reduce(0, (accumulator, value) -> accumulator += value);
        }

        return result;
    }

    private String[] splitParameters(String input) {
        Pair<List<String>, String> extractedDelimiter = extractDelimiterList(input);

        List<String> delimiterList = new LinkedList<>(DEFAULT_DELIMITER_LIST);
        if (extractedDelimiter.getKey() != null) {
            delimiterList.addAll(extractedDelimiter.getKey());
            input = extractedDelimiter.getValue();
        }

        String regex =
            delimiterList
                .stream()
                .reduce("", (accumulator, value) -> {
                    if (accumulator.length() == 0) {
                        accumulator = accumulator.concat(Pattern.quote(value));
                    }
                    else {
                        accumulator = accumulator.concat("|" + Pattern.quote(value));
                    }

                    return accumulator;
                });

        return input.split(regex);
    }

    private Pair<List<String>, String> extractDelimiterList(String input) {
        List<String> delimiterList = new LinkedList<>();
        int index = input.indexOf("\n");

        if (index > -1) {
            String firstLine = input.substring(0, index);

            if (firstLine.startsWith("//[")) {
                delimiterList = extractDelimiterListFromFirstLine(firstLine);
                input = input.replace(firstLine + "\n", "");
            }
        }

        return new Pair<>(delimiterList, input);
    }

    private List<String> extractDelimiterListFromFirstLine(String firstLine) {
        firstLine = firstLine.substring(3, firstLine.length() - 1);

        List<String> delimiterList = new LinkedList<>();
        String[] delimiters = firstLine.split(Pattern.quote("]["));

        delimiterList.addAll(Arrays.asList(delimiters));

        return delimiterList;
    }

    private List<Integer> validateParameters(String[] parameters) {
        List<Integer> negativeNumberList = new LinkedList<>();

        List<Integer> validatedParameterList =
            Arrays
                .stream(parameters)
                .map(value -> Integer.parseInt(value))
                .peek(value -> {
                    if (value < 0) negativeNumberList.add(value);
                })
                .collect(Collectors.toList());

        if (!negativeNumberList.isEmpty()) {
            throw new IllegalArgumentException(getNegativeNumbersErrorMessage(negativeNumberList));
        }

        return validatedParameterList;
    }

    private String getNegativeNumbersErrorMessage(List<Integer> negativeNumberList) {
        return
            negativeNumberList
                .stream()
                .map(value -> Integer.toString(value))
                .reduce(MESSAGE_NEGATIVE_NUMBERS_NOT_ALLOWED, (accumulator, value) -> accumulator + " " + value);
    }
}
