import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class StringCalculatorShould {

    private StringCalculator stringCalculator;

    @Before
    public void setUp() {
        stringCalculator = new StringCalculator();
    }

    @Test
    public void add_any_amount_of_numbers() {
        assertThat(stringCalculator.add("")).isEqualTo(0);
        assertThat(stringCalculator.add("1")).isEqualTo(1);
        assertThat(stringCalculator.add("1,2")).isEqualTo(3);
        assertThat(stringCalculator.add("1,2,3")).isEqualTo(6);
        assertThat(stringCalculator.add("1,2,3,4")).isEqualTo(10);
    }

    @Test
    public void accept_line_break_separator() {
        assertThat(stringCalculator.add("1\n2")).isEqualTo(3);
    }

    @Test
    public void accept_any_other_delimiter_specified_as_valid() {
        assertThat(stringCalculator.add("//[;]\n1;2")).isEqualTo(3);
    }

    @Test
    public void not_accept_negative_numbers() {
        Throwable throwable =
                catchThrowable(() -> stringCalculator.add("//[;]\n-1;2;-3"));

        assertThat(throwable).hasMessageContaining("negatives not allowed");
        assertThat(throwable).hasMessageContaining("-1");
        assertThat(throwable).hasMessageContaining("-3");
    }

    @Test
    public void ignore_numbers_bigger_than_1000() {
        assertThat(stringCalculator.add("//[;]\n1001;2")).isEqualTo(2);
    }

    @Test
    public void allow_delimiters_of_any_length_specified_by_brackets() {
        assertThat(stringCalculator.add("//[***]\n1***2***3")).isEqualTo(6);
    }

    @Test
    public void allow_multiple_delimiters() {
        assertThat(stringCalculator.add("//[*][%]\n1*2%3")).isEqualTo(6);
        assertThat(stringCalculator.add("//[***][||]\n1***2||3")).isEqualTo(6);
    }
}
